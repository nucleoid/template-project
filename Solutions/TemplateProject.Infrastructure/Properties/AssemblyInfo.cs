﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TemplateProject.Infrastructure")]
[assembly: AssemblyDescription("TemplateProject.Infrastructure")]

[assembly: InternalsVisibleTo("TemplateProject.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]