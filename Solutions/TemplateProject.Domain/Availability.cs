﻿
namespace TemplateProject.Domain
{
    public enum Availability
    {
        Online,
        Store,
        ThirdParty
    }
}
